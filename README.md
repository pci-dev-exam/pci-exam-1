# README #

PCI Dev team examination

### What is this repository for? ###

* Examination for PCI Dev

### Instructions ###

* Make a Task Management System with CRUD functionality you can use Vue or JQuery for modifications the pages are as follows:
    * All Tasks
    * Tasks per user/person
    * Create Task
    * Update Task
    * Delete Task
    * Update Profile with image upload
    * Notification to user if user had been assigned to a task (real-time or not real-time both do-able)

* you can route your functions via api.php or web.php (your choice)
* you can use composer packages
* after you are done with the exam just compress the project to a zip/rar/tar/7z then use https://wetransfer.com/ to transfer it to my email it@pcitech.com.ph
* Good Luck! :)

### How do I get set up? ###

* Same procedure on how to clone a laravel app

### Who do I talk to? ###

* You can message us in messenger for questions https://www.messenger.com/t/Duane.Five or https://www.messenger.com/t/tuksXD
* or email us at it@pcitech.com.ph